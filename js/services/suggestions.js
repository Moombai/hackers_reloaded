app.factory('suggestions', [function(){
	var demoSuggestions = {
		posts: [
		{
			title: 'Eloquent JavaScript, Second Edition',
			upvotes: 24,
			comments: [
			{
				body: 'I just wish we had hi-res, color, e-ink monitors with quick refresh rate. I cant bring myself to read an entire book online; even if its free.',
				upvotes: 41,
			},
			{
				body: 'Me too. I hate the eye-strain that the iPad causes when I use it outside in nature. Sometimes I go back to my Kindle, but it is too slow and too small for my complex PDFs.',
				upvotes: 24,
			},
			{
				body: 'Currently the biggest ebook reader is the Sony 13.3 incher but it is hard to obtain and quite expensive:',
				upvotes: 5,
			},
			{
				body: 'Currently the biggest ebook reader is the Sony 13.3 incher but it is hard to obtain and quite expensive:',
				upvotes: 5,
			}
			],
		},
		{
			title: 'Why do so many technical recruiters suck?',
			upvotes: 13,
			comments: [
			{
				body: 'For the same reason most (if not all) real estate agents are worthless. There iss no barrier to entry plus the allure of easy money. Until the profits collapse the market will be inundated with wannabes.',
				upvotes: 48,
			},
			{
				body: 'Since remunerative reward is based on quantity, not quality, market incentives are diametrically at odds with what both hiring managers and talent would benefit by.',
				upvotes: 20,
			},
			{
				body: 'I remember back around the year 2000 or so, I had a recruiter tell me they were looking for someone with a minimum of 10 years of Java experience, despite of the fact that Java was not created by James Gosling until 1994.',
				upvotes: 17,
			},
			{
				body: 'I am waiting for Google Voice or something similar to get here so that I can have all calls from an unknown number go straight to voicemail (including not actually ringing).',
				upvotes: 12,
			},
			{
				body: 'A friend of mine used a recruiting firm to find a position. I ended up providing a reference for him through the firm and spoke to a few individuals from that firm while he was trying to find a position. All I can say is I am glad he ended up finding a job because the recruiting firm was filled with idiots that really had no technical competence whatsoever. I get the impression this is a common problem with technical recruiters and has already tainted the potential credibility of anyone in this profession in my opinion.',
				upvotes: 10,
			},
			{
				body: 'I get this sort of crap from LinkedIn on a regular basis now.',
				upvotes: 5,
			}
			],
		},
		{
			title: 'How do I think in AngularJS if I have a jQuery background?',
			upvotes: 2,
			comments: [],
		},
	  ]
	}; 
	return demoSuggestions;
}])